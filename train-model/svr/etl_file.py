import pymongo
import csv


def etl_reg():
    print("Cargando registro....")
    connection = pymongo.MongoClient("mongodb://127.0.0.1")
    db_conexion = connection['atencion-hospitalaria']

    with open("files/data6.csv", encoding="utf8") as f:
        spamreader = csv.reader(f, delimiter=',')
        for i in spamreader:
            cod_esp = db_conexion.especialidades.find_one({"especialidad": i[7].lower()}, {"_id": 1})
            cod_diag = db_conexion.diagnosticos.find_one({"cod": i[9][:3]}, {"_id": 1})

            # cluster edad
            # 1: menores a 5 años
            # 2: entre 6 a 15 años
            # 3: entre 16 a 20 años
            # 4: entre 21 a 30 años
            # 5: entre 31 a 50 años
            # 6: mayores a 50 años

            ny = int(i[4])
            if ny <= 5:
                edad = 1
            elif ny <= 15 and ny > 5:
                edad = 2
            elif ny <= 20 and ny > 15:
                edad = 3
            elif ny <= 30 and ny > 20:
                edad = 4
            elif ny <= 50 and ny > 31:
                edad = 5
            elif ny > 50:
                edad = 6

            if cod_diag:
                reg_h = {"hcli": i[0],
                         "nac": i[1],
                         "pais": i[2].lower(),
                         "sexo": i[3],  # 1 hombre -- 2 mujer
                         "edad": edad,
                         "cod_prov": i[5],
                         "prov": i[6].lower(),
                         "especialidad": cod_esp["_id"],
                         "cod_inec": i[8],
                         "cod_diag": cod_diag["_id"],
                         "f_ingreso": i[10],
                         "f_egreso": i[11],
                         "estancia": int(i[12]),
                         }

                db_conexion.registro.insert_one(reg_h)


def etl_esp():
    print("Cargando registro....")
    connection = pymongo.MongoClient("mongodb://127.0.0.1")
    db_conexion = connection['atencion-hospitalaria']
    esp = set()

    with open("files/especialidades.txt", encoding="utf8") as f:
        for i in f:
            esp.add(i.lower().strip("\n"))

    j = 1

    for e in esp:
        esp_h = {"_id": j,
                 "especialidad": e
                 }
        db_conexion.especialidades.insert_one(esp_h)
        j = j + 1


def etl_diag():
    print("Cargando diagnósticos....")
    connection = pymongo.MongoClient("mongodb://127.0.0.1")
    db_conexion = connection['atencion-hospitalaria']

    with open("files/diagnosticos.csv", encoding="utf8") as f:
        spamreader = csv.reader(f, delimiter=';')
        diag_l = set()
        for i in spamreader:
            a = i[0].split(".")
            b = i[1].lower().split(",")

            diag_l.add(str(a[0] + ';' + b[0]))

    cont = 1
    for k in diag_l:
        div = k.split(";")
        diag = {"_id": cont,
                "cod": str(div[0]),
                "desc": str(div[1])
                }
        db_conexion.diagnosticos.insert_one(diag)
        cont = cont + 1

etl_esp()
etl_diag()
etl_reg()
