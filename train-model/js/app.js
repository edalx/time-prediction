angular.module('timePrediction', ['ngMaterial', 'ngMessages'])
    .filter('humanizeInt', function () {
        return function (input) {
            input = input || 0;
            return Humanize.compactInteger(input, 1);
        };
    })
    .filter('diagnositicos', function () {
        return function (input) {
            input = parseInt(input) || 0;
            return (_.find(window.diagnositicosx, {_id: input}) || {desc: '-'}).desc;
        };
    })
    .controller('AppCtrl', function ($scope, $http) {

        $scope.especialidades = [];
        $scope.time = '-1';
        $scope.busy = true;

        $http.get('/data').then(function (resp) {
            $scope.busy = false;
            $scope.especialidades = resp.data.especialidades;
            $scope.provincias = resp.data.provincias;
            $scope.diagnositicosx = resp.data.diagnositicos;

            window.especialidades = $scope.especialidades;
            window.provincias = $scope.provincias;
            window.diagnositicosx = $scope.diagnositicosx;

            $scope.calcularMetricas();
        }, function (e) {
            $scope.busy = false;
            $scope.especialidades = [{_id: 1, especialidad: 'esp1'}];
            $scope.provincias = [{cod_prov: 1, prov: 'prov1'}];
            $scope.diagnositicosx = [{_id: 1, desc: 'desc1'}];

            window.especialidades = $scope.especialidades;
            window.provincias = $scope.provincias;
            window.diagnositicosx = $scope.diagnositicosx;

            $scope.calcularMetricas();
        });


        $scope.calcularMetricas = function () {
            $scope.busy = true;
            $http.post('/metrics', {
                nac: $scope.nac,
                sexo: $scope.sexo,
                edad: $scope.edad,
                cod_prov: $scope.cod_prov,
                especialidad: $scope.especialidad,

            }).then(function (resp) {
                $scope.busy = false;
                $scope.metrics = resp.data;
                $scope.graficos();

            }, function (e) {
                $scope.busy = false;
                window.data = $scope.metrics;
                $scope.graficos();
            });

            $scope.predecir();
        };

        $scope.getDiagnostico = function () {
            $http.get('/diag?esp=' + $scope.especialidad, {
                especialidad: $scope.especialidad,

            }).then(function (resp) {
                $scope.busy = false;
                $scope.diagnositicos = resp.data.diagnositicos;
                $scope.calcularMetricas();

            }, function (e) {
                $scope.busy = false;
                window.data = $scope.metrics;
                $scope.graficos();
            });

            $scope.predecir();
        };

        $scope.predecir = function () {
            $scope.busy = true;
            $http.post('/time', {
                nac: $scope.nac,
                sexo: $scope.sexo,
                edad: $scope.edad,
                cod_prov: $scope.cod_prov,
                especialidad: $scope.especialidad,
                cod_diag: $scope.cod_diag,

            }).then(function (resp) {
                $scope.busy = false;
                $scope.time = resp.data.time;
            }, function (e) {
                $scope.busy = false;
                $scope.time = '-1';

            });
        }

        $scope.graficos = function () {
            var color = d3.scale.category20();

            var data1 = toArrayX($scope.metrics.nac, formatNacionalidad);
            c3.generate({
                bindto: '#c-nac',
                data: {
                    json: [data1.data],
                    type: 'pie',
                    keys: {
                        value: data1.value,
                    },
                    color: function (c, d) {
                        return color(d + 'xxx');
                    },
                }
            });

            var data2 = toArrayX($scope.metrics.edad, formatEdad);
            c3.generate({
                bindto: '#c-edad',
                data: {
                    json: [data2.data],
                    type: 'pie',
                    keys: {
                        value: data2.value,
                    }
                }
            });

            var data3 = toArrayX($scope.metrics.sexo, formatSexo);
            c3.generate({
                bindto: '#c-sexo',
                data: {
                    json: [data3.data],
                    type: 'donut',
                    keys: {
                        value: data3.value,
                    },
                    color: function (c, d) {
                        return color(d);
                    },
                }
            });

            var data4 = toArrayX($scope.metrics.provincias, formatProvincia);
            c3.generate({
                bindto: '#c-prov',
                data: {
                    json: [data4.data],
                    type: 'bar',
                    keys: {
                        value: data4.value,
                    },
                    // color: function (c, d) {
                    //     return color(d);
                    // },
                }
            });

            var data5 = toArrayX($scope.metrics.especialidad, formatEspecialidad);
            c3.generate({
                bindto: '#c-especialidad',
                data: {
                    json: [data5.data],
                    type: 'bar',
                    keys: {
                        value: data5.value,
                    },
                    // color: function (c, d) {
                    //     return color(d);
                    // },
                }
            });
        }

    });

function toArrayX(input, formatter) {
    var result = {data: {}, value: []};

    input.forEach(function (e) {
        let cod = formatter.apply(this, [e.cod.toString()]);
        result.value.push(cod);
        result.data[cod] = e.count;
    });

    return result;
}

function formatEspecialidad(input) {
    input = parseInt(input) || 0;
    return (_.find(window.especialidades, {_id: input}) || {especialidad: '-'}).especialidad;
}

function formatProvincia(input) {
    input = input.toString() || 0;
    return (_.find(window.provincias, {cod_prov: input}) || {prov: '-'}).prov;
}

function formatNacionalidad(input) {
    input = parseInt(input) || 0;
    switch (input) {
        case 1:
            return 'Ecuatoriano';
        case 2:
            return 'Extrangero';
    }
}

function formatEdad(input) {
    input = parseInt(input) || 0;
    switch (input) {
        case 1:
            return '<5';
        case 2:
            return '6-15';
        case 3:
            return '16-20';
        case 4:
            return '21-30';
        case 5:
            return '31-50';
        case 6:
            return '>50';

    }
}

function formatSexo(input) {
    input = parseInt(input) || 0;
    switch (input) {
        case 1:
            return 'Masculino';
        case 2:
            return 'Femenino';
    }
}