import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.linear_model import BayesianRidge
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy

import pymongo

print("Cargando datos....")
connection = pymongo.MongoClient("mongodb://127.0.0.1")
db_conexion = connection['atencion-hospitalaria']
query = list(db_conexion.registro.find({}))
X = []
y = []

data = []
for i in query:
    # features
    feat = [
        int(i['nac']),  # 1 nacional -- 2 extrangero
        int(i['sexo']),  # 1 hombre -- 2 mujer
        int(i['edad']),
        int(i['cod_prov']),
        int(i['especialidad']),
        int(i['cod_diag']),
    ]
    X.append(feat)
    y.append(int(i['estancia']))

# Dividir 80/20
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Entrenar el modelo
# Regresion lineal con mínimos cuadrados ordinarios
regr_o = LinearRegression()
regr_r = Ridge()
regr_l = Lasso()
regr_b = BayesianRidge()

regr_o.fit(X_train, y_train)
regr_r.fit(X_train, y_train)
regr_l.fit(X_train, y_train)
regr_b.fit(X_train, y_train)

pred_o = regr_o.predict(X_test)
pred_r = regr_r.predict(X_test)
pred_l = regr_l.predict(X_test)
pred_b = regr_b.predict(X_test)

print('Cross table:\n\n')
print("LinearRegression: \n")
df_o = pd.DataFrame({'Actual': y_test, 'Predecido': pred_o})
print(df_o)
print('\n')

print("Ridge:  \n")
df_r = pd.DataFrame({'Actual': y_test, 'Predecido': pred_o})
print(df_r)
print('\n')

print("Lasso:  \n")
df_l = pd.DataFrame({'Actual': y_test, 'Predecido': pred_o})
print(df_r)
print('\n')

print("BayesianRidge:  \n")
df_b = pd.DataFrame({'Actual': y_test, 'Predecido': pred_o})
print(df_r)
print('\n')

# R² score percentage of explained variance of the predictions
print("Certeza Regresión lineal: ")
print("LinearRegression: ")
scores_o = regr_o.score(X_test, y_test)
print(scores_o)
print("\n")
print("Ridge: ")
scores_r = regr_r.score(X_test, y_test)
print(scores_r)
print("\n")
print("Lasso: ")
scores_l = regr_l.score(X_test, y_test)
print(scores_l)
print("\n")
print("BayesianRidge: ")
scores_b = regr_b.score(X_test, y_test)
print(scores_b)

# plot graphs
t = list(range(1, 6083))
plt.scatter(t, y_test, label='Real')
plt.scatter(t, pred_o, label='Predict')
plt.legend()
plt.title('Regresión lineal múltiple')
plt.show()
