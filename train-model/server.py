"""
reference https://www.analyticsvidhya.com/blog/2017/09/machine-learning-models-as-apis-using-flask
"""
import os

from svr import api as time_predict
from flask import Flask, jsonify, request, render_template, send_from_directory
from flask_cors import CORS

# initialize API's
time_predict.initialize()

app = Flask(__name__, static_url_path='')

CORS(app)

jinja_options = app.jinja_options.copy()

jinja_options.update(dict(
    block_start_string='<%',
    block_end_string='%>',
    variable_start_string='%%',
    variable_end_string='%%',
    comment_start_string='<#',
    comment_end_string='#>'
))
app.jinja_options = jinja_options


@app.route('/time', methods=['POST'])
def enrichment():
    try:
        params = request.get_json()
        nac = params.get('nac')
        sexo = params.get('sexo')
        edad = params.get('edad')
        cod_prov = params.get('cod_prov')
        especialidad = params.get('especialidad')
        cod_diag = params.get('cod_diag')

        time = time_predict.time_prediction(nac, sexo, edad, cod_prov, especialidad, cod_diag)
        responses = jsonify(time=time)
        responses.status_code = 200

    except Exception as inst:
        print("Oops!  That was no valid number.  Try again...")
        responses = jsonify({'gender': 0, 'sent': 8, 'location': None, 'text': str(inst)})
        responses.status_code = 500

    return responses


@app.route('/data')
def data():
    try:
        dta = time_predict.data_api()
        responses = jsonify(dta)
        responses.status_code = 200

    except Exception as inst:
        print("Oops!  That was no valid number.  Try again...")
        responses = jsonify({"especialidades": None, "provincias": None, "diagnositicos": None})
        responses.status_code = 500

    return responses


@app.route('/diag')
def diag():
    try:
        esp = request.args.get('esp')
        dta = time_predict.diag_esp(esp)
        responses = jsonify(dta)
        responses.status_code = 200

    except Exception as inst:
        print("Oops!  That was no valid number.  Try again...")
        responses = jsonify({"especialidades": None, "provincias": None, "diagnositicos": None})
        responses.status_code = 500

    return responses


@app.route('/metrics', methods=['POST'])
def metrics():
    try:
        params = request.get_json()
        nac = params.get('nac')
        sexo = params.get('sexo')
        edad = params.get('edad')
        cod_prov = params.get('cod_prov')
        especialidad = params.get('especialidad')
        dta = time_predict.top_diag(nac, sexo, edad, cod_prov, especialidad)
        responses = jsonify(dta)
        responses.status_code = 200

    except Exception as inst:
        print("Oops!  That was no valid number.  Try again...")
        responses = jsonify({'gender': 0, 'sent': 8, 'location': None, 'text': inst})
        responses.status_code = 500

    return responses


@app.route('/')
def root():
    dir = os.path.dirname(__file__)
    file = os.path.join(dir, 'index.html')
    return render_template('index.html')


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)


if __name__ == "__main__":
    app.run()
