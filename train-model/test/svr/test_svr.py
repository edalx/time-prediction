from sklearn import svm
from sklearn.model_selection import train_test_split

import pymongo
import pandas

import matplotlib.pyplot as plt

print("Cargando datos....")
connection = pymongo.MongoClient("mongodb://127.0.0.1")
db_conexion = connection['atencion-hospitalaria']
query = list(db_conexion.registro.find({}))
X = []
y = []

data = []
for i in query:
    # features
    feat = [
        int(i['nac']),  # 1 nacional -- 2 extrangero
        int(i['sexo']),  # 1 hombre -- 2 mujer
        int(i['edad']),
        int(i['cod_prov']),
        int(i['especialidad']),
        int(i['cod_diag']),
    ]
    X.append(feat)
    y.append(int(i['estancia']))

# Dividir 80/20
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Entrenar el modelo
# Encontramos la profundidad del árbol donde la certeza se maximiza
regr_1 = svm.SVR()
regr_1.fit(X_train, y_train)
y_1 = regr_1.predict(X_test)
scores = regr_1.score(X_test, y_test)

df = pandas.DataFrame({'Actual': y_test, 'Predecido': y_1})
print('\n')
print('Cross table:')
print(df)
print('\n')

# Media cuadrática
# El coeficiente R ^ 2 se define como (1 - u / v), donde u es la suma de cuadrados residuales
# ((y_true - y_pred) ** 2) .sum () yv es la suma total de cuadrados ((y_true - y_true.mean ()) ** 2) .sum ().
# La mejor puntuación posible es 1.0 y puede ser negativa (porque el modelo puede ser arbitrariamente peor).
# Un modelo constante que siempre predice el valor esperado de y, sin tener en cuenta las características de entrada,
#  obtendría una puntuación R ^ 2 de 0.0.
print("Returns the coefficient of determination R^2 of the prediction." + str(regr_1.score(X_test, y_test)))

# plot graphs
t = list(range(1, 6083))
plt.scatter(t, y_test, label='Real')
plt.scatter(t, y_1, label='Predict')
plt.legend()
plt.title('Support Vector Regression')
plt.show()