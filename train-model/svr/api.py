import os
import pymongo
import dill as pickle
import math

filename = 'model_v1.pk'
global model
global db_conexion


def initialize():
    global model
    global db_conexion
    connection = pymongo.MongoClient("mongodb://127.0.0.1")
    db_conexion = connection['atencion-hospitalaria']
    dir = os.path.dirname(__file__)
    file = os.path.join(dir, 'files/' + filename)
    with open(file, 'rb') as f:
        model = pickle.load(f)


#
# i['nac'],  # 1 nacional -- 2 extrangero
# i['sexo'],  # 1 hombre -- 2 mujer
# i['edad'],
# i['cod_prov'],
# i['especialidad'],
# i['cod_diag'],


def time_prediction(nac, sexo, edad, cod_prov, especialidad, cod_diag):
    pred = model.predict([[nac, sexo, edad, cod_prov, especialidad, cod_diag]])
    return math.ceil(pred[0])


def avg_diag():
    pipeline = [
        {"$group": {"_id": "$cod_diag", "avgTime": {"$avg": "$estancia"}}}
    ]
    db_conexion.registro.aggregate(pipeline)


def avg_diag_cod(cod):
    pipeline = [
        {"$match": {"cod_diag": int(cod)}},
        {"$group": {"_id": "$cod_diag", "avgTime": {"$avg": "$estancia"}}}
    ]
    db_conexion.registro.aggregate(pipeline)


def avg_esp():
    pipeline = [
        {"$group": {"_id": "$especialidad", "avgTime": {"$avg": "$estancia"}}}
    ]
    db_conexion.registro.aggregate(pipeline)


def avg_esp_cod(cod):
    pipeline = [
        {"$match": {"especialidad": int(cod)}},
        {"$group": {"_id": "$especialidad", "avgTime": {"$avg": "$estancia"}}}
    ]
    db_conexion.registro.aggregate(pipeline)


def data_api():
    esp = list(db_conexion.especialidades.find({}, {"especialidad": "1"}).sort([("especialidad", pymongo.ASCENDING)]))
    # prov = list(db_conexion.registro.find({}, {"prov": 1, "cod_prov": 1}).distinct("prov"))
    prov = list(db_conexion.registro.aggregate(
        [
            {"$group": {"_id": {"prov": "$prov", "cod_prov": "$cod_prov"}}},
            {"$project": {"prov": "$_id.prov", "cod_prov": "$_id.cod_prov", "_id": False}}
        ]
    ))
    diag = list(db_conexion.diagnosticos.find({}, {"cod": 0}))
    return {"especialidades": esp, "provincias": sorted(prov, key=lambda p: p['prov']), "diagnositicos": diag}


def diag_esp(id_esp):
    if id_esp is not None:
        diag_cods = list(db_conexion.registro.find({'especialidad': int(id_esp)}, {'cod_diag': 1}).distinct("cod_diag"))
        diag = list(
            db_conexion.diagnosticos.find({"_id": {"$in": diag_cods}}, {"cod": 0}).sort([("desc", pymongo.ASCENDING)]))
    else:
        diag = list()

    return {"diagnositicos": diag}


def top_diag(nac=None, sexo=None, edad=None, cod_prov=None, especialidad=None):
    temp = db_conexion.registro.find(
        {"nac": str(nac), "sexo": str(sexo), "edad": edad, "cod_prov": str(cod_prov), "especialidad": especialidad},
        {"cod_diag": 1, "_id": 1}).distinct("cod_diag")

    # Metrics/////
    match = {}
    if nac is not None and len(nac) > 0:
        match["nac"] = nac

    pipeline = [
        {"$match": match},
        {"$group": {"_id": "$nac", "count": {"$sum": 1}}},
        {"$project": {"cod": "$_id", "count": "$count", "_id": False}}
    ]
    res_nac = list(db_conexion.registro.aggregate(pipeline))

    pipeline = [
        {"$match": match},
        {"$group": {"_id": "$sexo", "count": {"$sum": 1}}},
        {"$project": {"cod": "$_id", "count": "$count", "_id": False}}
    ]
    res_sexo = list(db_conexion.registro.aggregate(pipeline))

    pipeline = [
        {"$match": match},
        {"$group": {"_id": "$edad", "count": {"$sum": 1}}},
        {"$project": {"cod": "$_id", "count": "$count", "_id": False}}
    ]
    res_edad = list(db_conexion.registro.aggregate(pipeline))

    pipeline = [
        {"$match": match},
        {"$group": {"_id": "$cod_prov", "count": {"$sum": 1}}},
        {"$sort": {"count": -1}},
        {"$limit": 10},
        {"$project": {"cod": "$_id", "count": "$count", "_id": False}}
    ]
    res_provincias = list(db_conexion.registro.aggregate(pipeline))

    pipeline = [
        {"$match": match},
        {"$group": {"_id": "$especialidad", "count": {"$sum": 1}}},
        {"$sort": {"count": -1}},
        {"$limit": 10},
        {"$project": {"cod": "$_id", "count": "$count", "_id": False}}
    ]
    res_especialidad = list(db_conexion.registro.aggregate(pipeline))

    pipeline = [
        {"$match": match},
        {"$group": {"_id": "$cod_diag", "count": {"$sum": 1}, "time": {"$avg": "$estancia"}}},
        {"$sort": {"time": -1, "_id": 1}},
        {"$limit": 20},
        {"$project": {"cod": "$_id", "count": "$count", "time": "$time", "_id": False}}
    ]
    res_diagnositicos = list(db_conexion.registro.aggregate(pipeline))

    res = {"nac": res_nac, "sexo": res_sexo, "edad": res_edad, "provincias": res_provincias,
           "especialidad": res_especialidad,
           "diagnositicos": res_diagnositicos}
    print(res)
    return res


initialize()
top_diag()
