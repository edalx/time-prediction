from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split

import pymongo
import pickle

print("Cargando datos....")
connection = pymongo.MongoClient("mongodb://127.0.0.1")
db_conexion = connection['atencion-hospitalaria']
query = list(db_conexion.registro.find({}))
X = []
y = []
filename = 'model_v1.pk'

data = []
for i in query:
    # features
    feat = [
        int(i['nac']),  # 1 nacional -- 2 extrangero
        int(i['sexo']),  # 1 hombre -- 2 mujer
        int(i['edad']),
        int(i['cod_prov']),
        int(i['especialidad']),
        int(i['cod_diag']),
    ]
    X.append(feat)
    y.append(int(i['estancia']))

# Dividir 80/20
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Entrenar el modelo
# Encontramos la profundidad del árbol donde la certeza se maximiza
svr = DecisionTreeRegressor(max_depth=409)
svr.fit(X, y)

''' persistir modelo
'''
with open('./files/' + filename, 'wb') as file:
    pickle.dump(svr, file)

# '''
# Test predicción de permanencia hospitalaria
# '''
#
# while True:
#     print("Ingrese los valores:")
#     print("Ingrese nac: ")
#     a = input()
#     print("Ingrese sexo: ")
#     b = input()
#     print("Ingrese edad: ")
#     c = input()
#     print("Ingrese cod_prov: ")
#     d = input()
#     print("Ingrese especialidad: ")
#     e = input()
#     print("Ingrese cod_diag: ")
#     f = input()
#     print(svr.predict([[a, b, c, d, e, f]]))
